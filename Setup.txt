COMO REPRODUZIR A SEUMANUELWEBAPI NO SEU AMBIENTE

I. Criando o ambiente

1. Crie um banco de dados nomeado SeuManuelDB no SQL Server.

2. Importe o arquivo 'manuel.mdf' para o banco criado ou rode o arquivo 'manuel.sql' para criar as tabelas adequadas.

3. Abra a solu��o SeuManuelWebStore\SeuManuelWebStore.sln no Visual Studio 2017.

4. Instale um aplicativo de testes de Web Services, como SOAP UI ou Postman.

5. Inicie o servi�o IIS do seu computador atrav�s do Debug (F5) do Visual Studio, janela de comando ou outra maneira.



II. Refer�ncias

O projeto SeuManuelWebAPI faz refer�ncia a outro projeto na mesma solu��o, chamado SeuManuelDataAccess. Se, por alguma raz�o, essa rela��o for perdida, ser� necess�rio refaz�-la, abrindo o Solution Explorer do Visual Studio, depois em SeuManuelWebaPI -> References -> Bot�o Direito -> Add Reference.

Na aba Projects, selecione o SeuManuelDataAccess e confirme.



III. NuGet Package Manager

Alguns pacotes e bibliotecas externas foram utilizados na constru��o da Web API, entre eles Entity Framework, NetwonSoft JSON e System.ValueTuple.

Para instalar cada um, selecione o projeto SeuManuelWebAPI e em Tools -> NuGet Package Manager -> Package Manager Console, rode os comandos a seguir:

Entity Framework: Install-Package EntityFramework -Version 6.2.0
NetwonSoft JSON: Install-Package Newtonsoft.Json -Version 12.0.1	
System.ValueTuple: Install-Package System.ValueTuple -Version 4.5.0	



IV. Iniciando e testando o projeto

1. Na janela do aplicativo instalado no item 4 da cria��o de ambiente, crie um projeto REST e escreva como endpoint: http://localhost:XXXXX/api, onde XXXXX � a porta assinalada pelo seu host ou PC.

2. Para testar os servi�os, escolha o URL seguindo o padr�o:

	Produtos: /produtos/{MODULO}/{PAR�METROS}
	Pedidos: /pedidos/{MODULO}/{PAR�METROS}
	Relat�rios: /relatorios/{MODULO}/{PAR�METROS}

3. As refer�ncias de cada m�dulo e par�metro se encontram no arquivo 'Modulos.txt'.

4. Os dados de entrada (sample data) gerados pelo sistema se encontram no arquivo 'SampleData.txt'.

5. Cada a��o cont�m dados de entrada com um padr�o bem parecido para evitar confus�o, mas fique atento com a sintaxe e �tima integra��o!
