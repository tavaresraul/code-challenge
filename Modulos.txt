##################################################
##						##
##                  PRODUTOS                    ##
##						##
##################################################

Metodo HTTP: GET
URL: /api/produtos/ConsultarProdutos
Descricao: rotina de consulta de todos os produtos

Metodo HTTP: GET
URL: /api/produtos/ConsultarProdutosPorCodigo/{id}
Descricao: rotina de consulta de um produto especifico

Metodo HTTP: POST
URL: /api/produtos/CadastrarProduto
Descricao: rotina de cadastro de um unico produto

Metodo HTTP: POST
URL: /api/produtos/CadastrarProdutoMulti
Descricao: rotina de cadastro de multiplos produtos

Metodo HTTP: DELETE
URL: /api/produtos/RemoverProduto/{id}
Descricao: rotina de remocao de um unico produto

Metodo HTTP: PUT 
URL: /api/produtos/AtualizarProduto/{id}
Descricao: rotina de alteracao de um unico produto

Metodo HTTP: PUT
URL: /api/produtos/AtualizarProdutoMulti
Descricao: rotina de alteracao de multiplos produtos



##################################################
##						##
##                 ATRIBUTOS                    ##
##						##
##################################################

Metodo HTTP: GET
URL: api/atributos/ConsultarAtributosProduto/{id}
Descricao: rotina de consulta de todos os atributos de um produto

Metodo HTTP: POST
URL: api/atributos/CadastrarAtributosProduto
Descricao: rotina de cadastro de um atributo para um unico produto 

Metodo HTTP: POST
URL: api/atributos/CadastrarAtributosProdutoMulti
Descricao: rotina de cadastro de um ou mais atributos para um ou varios produtos

Metodo HTTP: DELETE
URL: api/atributos/RemoverAtributosProduto/{id}/{attribID}
Descricao: rotina de remocao de um atributo de um produto

Metodo HTTP: DELETE
URL: api/atributos/RemoverAtributosProdutoMulti/{id}
Descricao: rotina de remocao de todos os atributos de um produto

Metodo HTTP: PUT 
URL: api/atributos/AtualizarAtributosProduto/{id}
Descricao: rotina de atualizacao de um atributo especifico de um produto

Metodo HTTP: PUT
URL: api/atributos/AtualizarAtributosProdutoMulti
Descricao: rotina de atualizacao de um ou mais atributos de um produto



##################################################
##						##
##                   PEDIDOS                    ##
##						##
##################################################

Metodo HTTP: GET
URL: /api/pedidos/ConsultarPedidos
Descricao: rotina de consulta de todos os pedidos

Metodo HTTP: GET
URL: /api/pedidos/ConsultarPedidosPorNumero/{id}
Descricao: rotina de consulta de um pedido especifico

Metodo HTTP: POST
URL: /api/pedidos/CadastrarPedidos
Descricao: rotina de cadastro de um unico pedido

Metodo HTTP: POST
URL: /api/pedidos/CadastrarPedidosMulti
Descricao: rotina de cadastro de um ou mais pedidos

Metodo HTTP: DELETE
URL: /api/pedidos/RemoverPedido/{id}
Descricao: rotina de exclusao de um pedido especifico

Metodo HTTP: PUT 
URL: /api/pedidos/AtualizarPedidos/{id}
Descricao: rotina de alteracao de um unico pedido

Metodo HTTP: PUT
URL: /api/pedidos/AtualizarPedidosMulti
Descricao: rotina de alteracao de dois ou mais pedidos



##################################################
##						##
##                RELATÓRIOS                    ##
##						##
##################################################


Metodo HTTP: GET
URL: /api/relatorios/TicketMedio/{date1}/{date2}/{metodo}
Descricao: // rotina que informa o ticket medio com opcao de selecao de metodologia