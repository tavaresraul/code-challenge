﻿using SeuManuelDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SeuManuelWebAPI.Controllers
{
    [RoutePrefix("api/produtos")]
    public class ProdutosController : ApiController
    {

        // PRODUTOS

        [AcceptVerbs("GET")] // rotina de consulta de todos os produtos
        [Route("ConsultarProdutos")] // cria URL com texto customizado
        public HttpResponseMessage ConsultarProdutos()
        {
            // usa a entidade de conexao com banco de dados
            using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
            {
                // desabilita o lazy loading
                entities.Configuration.LazyLoadingEnabled = false;
                entities.Configuration.ProxyCreationEnabled = false;

                // cria uma lista com todos os produtos
                var prodct = (from e in entities.Products
                              select new
                              {
                                  e.productCode,
                                  e.productName,
                                  e.productDesc,
                                  e.productInStock,
                                  e.productPrice,
                                  ProductsAttributes = e.ProductsAttributes.Select(a => new { a.attribID, a.attribName, a.attribDesc })
                              }).ToList();

                // valida se a lista é nula, ou seja, se existem produtos cadastrados
                if (prodct != null)
                {
                    // retorna a lista criada
                    return Request.CreateResponse(HttpStatusCode.OK, prodct);
                }
                else
                {
                    // devolve mensagem de erro ao usuário
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não foram encontrados produtos.");
                }

            }
        }


        [AcceptVerbs("GET")] // rotina de consulta de um produto específico
        [Route("ConsultarProdutosPorCodigo/{id}")]
        public HttpResponseMessage ConsultarProdutoPorCodigo(int id)
        {
            // usa a entidade de conexao com banco de dados
            using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
            {
                // desabilita o lazy loading
                entities.Configuration.LazyLoadingEnabled = false;
                entities.Configuration.ProxyCreationEnabled = false;

                // busca o produto especificado
                var prodct = (from e in entities.Products
                              where e.productCode == id.ToString()
                              select new
                              {
                                  e.productCode,
                                  e.productName,
                                  e.productDesc,
                                  e.productInStock,
                                  e.productPrice,
                                  ProductsAttributes = e.ProductsAttributes.Select(a => new { a.attribID, a.attribName, a.attribDesc })
                              }).FirstOrDefault();

                // valida se o elemento é nulo, ou seja, se existem produtos cadastrados
                if (prodct != null)
                {
                    // retorna o elemento encontrado
                    return Request.CreateResponse(HttpStatusCode.OK, prodct);
                }
                else
                {
                    // retorna erro de não encontrado
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Produto " + id + " não encontrado.");
                }
            }
        }


        [AcceptVerbs("POST")] // rotina de cadastro de um único produto
        [Route("CadastrarProduto")]
        public HttpResponseMessage CadastrarProduto([FromBody] Product product)
        {
            // protege a execução do bloco
            try
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {

                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // verifica se o produto já existe
                    var prodct = entities.Products.Where(p => p.productCode == product.productCode).Select(p => p.productCode).FirstOrDefault();

                    // se já existir
                    if (prodct != null)
                    {
                        // retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "O produto " + product.productCode + " já está cadastrado.");
                    }

                    // itera cada atributo
                    foreach (ProductsAttribute pa in product.ProductsAttributes)
                    {
                        // pequeno ajuste na lista de atributos para evitar redundância no input JSON
                        pa.productCode = product.productCode;

                        // insere cada atributo na tabela
                        entities.ProductsAttributes.Add(pa);
                    }

                    // insere o produto no banco de dados
                    entities.Products.Add(product);

                    // salva as alterações
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.Created, "Produto " + product.productCode + " inserido com sucesso.");
                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("POST")] // rotina de cadastro de múltiplos produtos
        [Route("CadastrarProdutoMulti")]
        public HttpResponseMessage CadastrarPedidoMulti([FromBody] List<Product> prds)
        {
            // protege a execução do bloco
            try
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {

                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // itera cada produto enviado pelo JSON
                    foreach (Product p in prds)
                    {

                        // verifica se o produto já existe
                        var prodct = entities.Products.Where(l => l.productCode == p.productCode).Select(l => l.productCode).FirstOrDefault();

                        // se já existir
                        if (prodct != null)
                        {
                            // retorna mensagem de erro
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "O produto " + p.productCode + " já está cadastrado.");
                        }

                        // itera cada atributo
                        foreach (ProductsAttribute pa in p.ProductsAttributes)
                        {
                            // pequeno ajuste na lista de atributos para evitar redundância no input JSON
                            pa.productCode = p.productCode;

                            // insere cada atributo na tabela
                            entities.ProductsAttributes.Add(pa);
                        }

                        // insere o produto e seus atributos no banco de dados
                        entities.Products.Add(p);

                    }

                    // salva as alterações
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.Created, "Produtos inseridos com sucesso");
                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("DELETE")] // rotina de remoção de um único produto
        [Route("RemoverProduto/{id}")]
        public HttpResponseMessage RemoverProduto(int id)
        {
            // protege a execução do bloco
            try
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {

                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // busca o produto para remoção
                    var prd = entities.Products.Include("ProductsAttributes").Where(e => e.productCode == id.ToString()).FirstOrDefault();

                    // verifica se o produto existe
                    if (prd != null)
                    {
                        //verifica se o produto tem atributos relacionados
                        if (prd.ProductsAttributes.Count() > 0)
                        {
                            // assinala os atributos para uma lista
                            var attributeList = prd.ProductsAttributes.ToList();

                            // itera todos os elementos da lista de atributos
                            foreach (ProductsAttribute pa in attributeList)
                            {
                                // remove os atributos da tabela
                                entities.ProductsAttributes.Remove(pa);
                            }

                        }

                        // remove o produto desejado sem relação com mais nenhum atributo
                        entities.Products.Remove(prd);

                        // salva as alterações
                        entities.SaveChanges();

                        // retorna mensagem de sucesso
                        return Request.CreateResponse(HttpStatusCode.OK, "Produto " + id + " removido com sucesso.");
                    }
                    else // caso o produto não existe
                    {
                        // retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Produto " + id + " não encontrado");
                    }

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }


        [AcceptVerbs("PUT")] // rotina de alteração de um único produto
        [Route("AtualizarProduto/{id}")]
        public HttpResponseMessage AtualizarProduto(int id, [FromBody]Product product)
        {

            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // busca o produto desejado
                    var prd = entities.Products.Include("ProductsAttributes").Where(e => e.productCode == id.ToString()).FirstOrDefault();

                    // verifica se o produto existe
                    if (prd == null)
                    {
                        // caso não exista, retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "O produto " + id.ToString() + " não existe.");

                    }
                    else // caso exista,
                    {
                        // atualiza as propriedades da entidade com as informações do input JSON
                        prd.productName = product.productName;
                        prd.productDesc = product.productDesc;
                        prd.productInStock = product.productInStock;
                        prd.productPrice = product.productPrice;

                        // antes de salvar, verifica se há atributos no JSON input
                        if (product.ProductsAttributes.Count() > 0)
                        {

                            // itera cada atributo do input JSON 
                            foreach (ProductsAttribute pa in product.ProductsAttributes)
                            {
                                // busca o atributo na tabela correspondente com Código do Produto e ID do atributo 
                                var attr = entities.ProductsAttributes.Where(a => (a.productCode == id.ToString()) && (a.attribID == pa.attribID)).FirstOrDefault();

                                // valida se o atributo existe no sistema
                                if (attr != null)
                                {
                                    // atualiza de acordo
                                    attr.attribName = pa.attribName;
                                    attr.attribDesc = pa.attribDesc;
                                }
                                else // caso não exista, adiciona automaticamente apenas se o ID não estiver sendo usado
                                {
                                    // busca o ID na tabela de atributos
                                    var usedAtt = entities.ProductsAttributes.Where(at => at.attribID == pa.attribID).Select(at => at).FirstOrDefault();

                                    // verifica se é nulo
                                    if (usedAtt == null)
                                    {

                                        // pequeno fix para evitar redundância nos atributos do input JSON, assinala o ProductCode da URL ao atributo vinculado
                                        pa.productCode = id.ToString();

                                        // adiciona o atributo na tabela
                                        entities.ProductsAttributes.Add(pa);

                                    }
                                    else // caso já exista
                                    {
                                        // retornar mensagem de erro
                                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "O atributo ID " + pa.attribID + " já está sendo usado por outro produto.");
                                    }

                                }
                            }
                        }

                        // grava todas as mudanças no banco de dados
                        entities.SaveChanges();

                        // retorna mensagem de sucesso
                        return Request.CreateResponse(HttpStatusCode.OK, "Produto " + id.ToString() + " + atributos atualizados com sucesso.");
                    }

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("PUT")] // rotina de alteração de múltiplos produtos
        [Route("AtualizarProdutoMulti")]
        public HttpResponseMessage AtualizarProdutoMulti([FromBody] List<Product> product)
        {

            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // itera cada produto postado no input JSON
                    foreach (Product p in product)
                    {
                        // busca o produto no banco de dados
                        var prd = entities.Products.Include("ProductsAttributes").Where(e => e.productCode == p.productCode).FirstOrDefault();

                        // valida se o produto existe
                        if (prd == null)
                        {
                            // caso não exista, retorna mensagem de erro
                            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "O produto " + p.productCode + "não existe.");

                        }
                        else // caso exista, 
                        {
                            // atualiza inicialmente o produto
                            prd.productName = p.productName;
                            prd.productDesc = p.productDesc;
                            prd.productInStock = p.productInStock;
                            prd.productPrice = p.productPrice;

                            // verifica se há atributos na atualização
                            if (p.ProductsAttributes.Count() > 0)
                            {

                                // itera cada atributo do input JSON 
                                foreach (ProductsAttribute pa in p.ProductsAttributes)
                                {
                                    // busca o atributo na tabela correspondente com Código do Produto e ID do atributo 
                                    var attr = entities.ProductsAttributes.Where(a => (a.productCode == p.productCode.ToString()) && (a.attribID == pa.attribID)).FirstOrDefault();

                                    // valida se o atributo existe no sistema
                                    if (attr != null)
                                    {
                                        // atualiza o atributo
                                        attr.attribName = pa.attribName;
                                        attr.attribDesc = pa.attribDesc;
                                    }
                                    else // caso não exista, adiciona automaticamente
                                    {
                                        // busca o ID na tabela de atributos
                                        var usedAtt = entities.ProductsAttributes.Where(at => at.attribID == pa.attribID).Select(at => at).FirstOrDefault();

                                        // verifica se é nulo
                                        if (usedAtt == null)
                                        {
                                            // pequeno fix para evitar redundância nos atributos do JSON, assinala o ProductCode da URL ao atributo vinculado
                                            pa.productCode = p.productCode.ToString();

                                            // adiciona o atributo automaticamente
                                            entities.ProductsAttributes.Add(pa);

                                        }
                                        else // caso já exista
                                        {
                                            // retornar mensagem de erro
                                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "O atributo ID " + pa.attribID + " já está sendo usado por outro produto.");
                                        }
                                    }
                                }
                            }
                        }

                    }

                    // salva as alterações no banco
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.OK, "Todos os produtos foram atualizados com sucesso");

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}
