﻿using SeuManuelDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SeuManuelWebAPI.Controllers
{
    [RoutePrefix("api/atributos")]
    public class AtributosController : ApiController
    {

        // ATRIBUTOS

        [AcceptVerbs("GET")] // rotina de consulta de todos os atributos de um produto
        [Route("ConsultarAtributosProduto/{id}")]
        public HttpResponseMessage ConsultarAtributosProduto(int id)
        {
            // usa a entidade de conexao com banco de dados
            using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
            {
                // desabilita o lazy loading
                entities.Configuration.LazyLoadingEnabled = false;
                entities.Configuration.ProxyCreationEnabled = false;

                // retorna lista com atributos do produto desejado
                var att = entities.ProductsAttributes.Where(e => e.productCode == id.ToString()).ToList();

                // confere se algum atributo existe
                if (att != null)
                {
                    // retorna lista com os atributos
                    return Request.CreateResponse(HttpStatusCode.OK, att);
                }
                else // caso não exista,
                {
                    // retorna mensagem de vazio
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existem atributos para o produto " + id + ".");
                }
            }
        }


        [AcceptVerbs("POST")] // rotina de cadastro de um atributo para um único produto 
        [Route("CadastrarAtributosProduto")]
        public HttpResponseMessage CadastrarAtributosProduto([FromBody] ProductsAttribute attr)
        {
            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {

                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // verifica se o atributo já existe
                    var att = entities.ProductsAttributes.Where(a => a.attribID == attr.attribID).Select(a => a).FirstOrDefault();

                    // se já existir
                    if (att != null)
                    {
                        // retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "O atributo " + att.attribID + " já existe. Escolha outro.");
                    }

                    // adiciona o atributo na tabela
                    entities.ProductsAttributes.Add(attr);

                    // salva as alterações
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.Created, "Atributo adicionado com sucesso no produto " + attr.productCode);

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("POST")]  // rotina de cadastro de um ou mais atributos para um ou vários produtos
        [Route("CadastrarAtributosProdutoMulti")]
        public HttpResponseMessage CadastrarAtributosProdutoMulti([FromBody] List<ProductsAttribute> attr)
        {
            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // itera cada atributo do input JSON 
                    foreach (ProductsAttribute pa in attr)
                    {

                        // verifica se o atributo já existe
                        var att = entities.ProductsAttributes.Where(a => a.attribID == pa.attribID).Select(a => a).FirstOrDefault();

                        // se já existir
                        if (att != null)
                        {
                            // retorna mensagem de erro
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "O atributo " + att.attribID + " já existe. Escolha outro.");
                        }

                        // adiciona o atributo na tabela
                        entities.ProductsAttributes.Add(pa);
                    }

                    // salva as alterações
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.Created, "Todos os atributos foram adicionandos com sucesso.");

                }

            }
            catch (Exception ex)
            // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("DELETE")] // rotina de remoção de um atributo de um produto
        [Route("RemoverAtributosProduto/{id}/{attribID}")]
        public HttpResponseMessage RemoverAtributosProduto(int id, int attribID)
        {
            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // retorna o atributo encontrado com ProductCode + ID do atributo
                    var attr = entities.ProductsAttributes.Where(e => (e.productCode == id.ToString()) && e.attribID == attribID).FirstOrDefault();

                    // valida se o elemento é nulo
                    if (attr != null)
                    {
                        // remove o atributo da tabela
                        entities.ProductsAttributes.Remove(attr);

                        // salva as alterações
                        entities.SaveChanges();

                        // retorna mensagem de sucesso
                        return Request.CreateResponse(HttpStatusCode.OK, "Atributo " + attribID.ToString() + " removido do produto " + id.ToString() + " com sucesso.");
                    }
                    else // caso nulo,
                    {
                        // retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "O atributo " + id + " não existe para o produto informado.");
                    }

                }

            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }


        [AcceptVerbs("DELETE")] // rotina de remoção de todos os atributos de um produto
        [Route("RemoverAtributosProdutoMulti/{id}")]
        public HttpResponseMessage RemoverAtributosProdutoMulti(int id)
        {
            try  // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {

                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // retorna lista de atributos do produto indicado
                    var attr = entities.ProductsAttributes.Where(e => e.productCode == id.ToString()).ToList();

                    // // valida se a lista é nula
                    if (attr != null)
                    {
                        // itera cada atributo do banco de dados
                        foreach (ProductsAttribute p in attr)
                        {
                            // remove o atributo da tabela
                            entities.ProductsAttributes.Remove(p);
                        }

                        //salva as alterações
                        entities.SaveChanges();

                        // retorna mensagem de sucesso
                        return Request.CreateResponse(HttpStatusCode.OK, "Todos os atributos do produto " + id + " foram removidos com sucesso.");
                    }
                    else // caso não nulo
                    {
                        // retorna mensagem de vazio
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Não existem atributos para o produto " + id + ".");
                    }

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }


        [AcceptVerbs("PUT")] // rotina de atualização de um atributo específico de um produto
        [Route("AtualizarAtributosProduto/{id}")]
        public HttpResponseMessage AtualizarAtributosProduto(int id, [FromBody]ProductsAttribute attr)
        {

            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // pequeno ajuste para evitar redundância de escrever o mesmo ID do URL no input JSON
                    attr.productCode = id.ToString();

                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // retorna o atributo desejado no banco de dados
                    var attrib = entities.ProductsAttributes.Where(e => e.attribID == attr.attribID).Where(e => e.productCode == id.ToString()).FirstOrDefault();

                    // valida se o elemento é nulo
                    if (attrib == null)
                    {
                        // retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "O atributo desejado " + id.ToString() + " não pode ser atualizado.");

                    }
                    else // caso não,
                    {

                        // atualiza os dados com as informaçõs do input JSON
                        attrib.attribName = attr.attribName;
                        attrib.attribDesc = attr.attribDesc;

                        // salva as alterações
                        entities.SaveChanges();

                        // retorna mensagem de sucesso
                        return Request.CreateResponse(HttpStatusCode.OK, "O atributo desejado " + attr.attribID + " foi atualizado para o produto " + id.ToString() + ".");
                    }

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("PUT")] // rotina de atualização de um ou mais atributos de um produto
        [Route("AtualizarAtributosProdutoMulti")]
        public HttpResponseMessage AtualizarAtributosProdutoMulti([FromBody]List<ProductsAttribute> attr)
        {

            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {

                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // itera cada atributo do input JSON 
                    foreach (ProductsAttribute pa in attr)
                    {
                        // pequeno ajuste para evitar redundância de escrever o mesmo ID do URL no input JSON
                        //pa.productCode = id.ToString();

                        // desabilita o lazy loading
                        entities.Configuration.LazyLoadingEnabled = false;
                        entities.Configuration.ProxyCreationEnabled = false;

                        // retorna o elemento encontrado
                        var attrib = entities.ProductsAttributes.Where(e => (e.attribID == pa.attribID) && (e.productCode == pa.productCode)).FirstOrDefault();

                        // valida se o elemento é nulo
                        if (attrib == null)
                        {
                            // retorna mensagem de vazio
                            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "O atributo " + pa.attribID.ToString() + " não existe.");

                        }
                        else // caso não nulo
                        {
                            // atualiza o valor atual com o informado pelo input JSON
                            attrib.attribName = pa.attribName;
                            attrib.attribDesc = pa.attribDesc;

                        }
                    }

                    // salva as alterações
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.OK, "Atributos atualizados com sucesso");

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
