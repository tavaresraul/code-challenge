﻿using SeuManuelDataAccess;
using SeuManuelWebAPI.Classes;
using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SeuManuelWebAPI.Controllers
{
    [RoutePrefix("api/relatorios")]
    public class RelatoriosController : ApiController
    {
        [AcceptVerbs("GET")] // rotina que informa o ticket médio com opção de seleção de metodologia
        [Route("TicketMedio/{date1}/{date2}/{metodo}")]
        public HttpResponseMessage TicketMedio(string date1, string date2, int metodo)
        {
            // usa a entidade de conexao com banco de dados
            using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
            {
                // inicia variável de ticket médio como 0
                double slsRevenue = 0;

                // desabilita o lazy loading
                entities.Configuration.LazyLoadingEnabled = false;
                entities.Configuration.ProxyCreationEnabled = false;

                // substitui traços por barras e inverte as datas do formato pt_BR para o formato en_US
                date1 = date1.Substring(6, 4) + "/" + date1.Substring(3, 2) + "/" + date1.Substring(0, 2);
                date2 = date2.Substring(6, 4) + "/" + date2.Substring(3, 2) + "/" + date2.Substring(0, 2);

                // define como cultura americana
                CultureInfo en_us = new CultureInfo("en-US");

                // transforma as datas formatadas em elementos de DateTime padrão en_US
                DateTime start = DateTime.Parse(date1, en_us);
                DateTime end = DateTime.Parse(date2, en_us);

                // buscar os Order IDs no período informado
                var orderList = entities.Orders.Where(o => (o.orderDate >= start) && (o.orderDate <= end)).Select(o => o.orderID).ToList();

                // calcula os valores totais dos fretes dos pedidos do período
                double shipTotal = (from sl in entities.Orders where orderList.Contains(sl.orderID) select sl.orderShipPrice).Sum();

                // retorna o valor de todos os itens dos pedidos
                double itemsTotal = (from ot in entities.OrdersItems where orderList.Contains(ot.orderID) select ot.productFinalPrice).Sum();

                // soma o valor dos items dos pedidos com o valor dos fretes, obtendo o valor TOTAL de todos os pedidos
                double orderTotal = itemsTotal + shipTotal;

                // obtém o ticket médio

                // METODOLOGIA 1: divide-se o valor de faturamento total de vendas pela quantidade de vendas
                if (metodo == 1)
                {
                    // obtem quantidade de vendas
                    int orderCount = orderList.Count();

                    // obtem ticket medio
                    slsRevenue = orderTotal / orderCount;

                }
                else // METODOLOGIA 2: divide-se o valor total de vendas pela quantidade de clientes que geraram o volume de vendas
                {
                    // obtem a quantidade de clientes distintos
                    int custCount = (from dc in entities.Orders where orderList.Contains(dc.orderID) select dc.orderCustID).Distinct().Count();

                    // obtem ticket medio
                    slsRevenue = orderTotal / custCount;

                }

                // define a cultura monetária como reais brasileiros
                CultureInfo reais = CultureInfo.CreateSpecificCulture("pt-BR");

                // utiliza a classe ticket medio para obter o objeto
                TicketMedio tm = new TicketMedio
                {
                    SlsRevenue = slsRevenue
                };

                // retorna o valor do ticket médio em R$
                return Request.CreateResponse(HttpStatusCode.OK, tm.SlsRevenue.ToString("C", reais));
                
            }
        }


    }

}
