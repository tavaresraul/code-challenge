﻿using SeuManuelDataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SeuManuelWebAPI.Controllers
{
    [RoutePrefix("api/pedidos")]
    public class PedidosController : ApiController
    {
        [AcceptVerbs("GET")] // rotina de consulta de todos os pedidos
        [Route("ConsultarPedidos")]
        public HttpResponseMessage ConsultarPedidos()
        {
            // usa a entidade de conexao com banco de dados
            using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
            {
                // desabilita o lazy loading
                entities.Configuration.LazyLoadingEnabled = false;
                entities.Configuration.ProxyCreationEnabled = false;

                // cria uma lista com todos os pedidos
                var orders = (from e in entities.Orders
                              select new
                              {
                                  e.orderID,
                                  e.orderDate,
                                  e.orderCustID,
                                  e.orderStatus,
                                  e.orderShipPrice,
                                  e.OrdersItems
                              }).ToList();

                // valida se a lista é nula
                if (orders != null)
                {
                    // retorna a lista criada
                    return Request.CreateResponse(HttpStatusCode.OK, orders);
                }
                else // caso não 
                {
                    // retorna mensagem de erro
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Could not retrieve all orders.");
                }

            }
        }


        [AcceptVerbs("GET")] // rotina de consulta de um pedido específico
        [Route("ConsultarPedidosPorNumero/{id}")]
        public HttpResponseMessage ConsultarPedidosPorNumero(int id)
        {
            // usa a entidade de conexao com banco de dados
            using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
            {
                // desabilita o lazy loading
                entities.Configuration.LazyLoadingEnabled = false;
                entities.Configuration.ProxyCreationEnabled = false;

                // retorna o elemento encontrado do banco de dados
                var ord = (from e in entities.Orders
                           where e.orderID == id
                           select new
                           {
                               e.orderID,
                               e.orderDate,
                               e.orderCustID,
                               e.orderStatus,
                               e.orderShipPrice,
                               e.OrdersItems
                           }).FirstOrDefault();

                // valida se o elemento é nulo
                if (ord != null)
                {
                    // retorna o elemento encontrado do banco de dados
                    return Request.CreateResponse(HttpStatusCode.OK, ord);
                }
                else // caso nulo
                {
                    // retorna erro de não encontrado
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "The requested Order " + id + " was not found.");
                }
            }
        }


        [AcceptVerbs("POST")]
        [Route("CadastrarPedidos")] // rotina de cadastro de um único pedido
        public HttpResponseMessage CadastrarPedidos([FromBody] Order order)
        {
            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // verifica se o orderID já existe
                    if (entities.Orders.Where(o => o.orderID == order.orderID).Select(o => o).FirstOrDefault() != null)
                    {
                        // retorna código de erro caso tente cadastrar pedido já existente
                        return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Já existe um pedido com esse ID no sistema");
                    }

                    // verifica se há itens atrelados ao pedido
                    if (order.OrdersItems.Count() == 0)
                    {
                        // retorna código de erro caso tente cadastrar pedido sem nenhum item atrelado
                        return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Não é possível adicionar um pedido sem itens.");
                    }

                    // itera cada item do input JSON
                    foreach (OrdersItem o in order.OrdersItems)
                    {
                        // confere se o item existe na tabela de produtos
                        var productExists = entities.Products.Where(x => x.productCode == o.productCode).Select(x => x).FirstOrDefault();

                        // valida se o produto existe
                        if (productExists == null)
                        {
                            //retorna mensagem de erro
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Pedido " + order.orderID + " não foi inserido pois o produto " + o.productCode + " não está cadastrado.");
                        }

                        // valida se o produto tem estoque
                        if (productExists.productInStock == 0 || o.productQty > productExists.productInStock)
                        {
                            //retorna mensagem de erro
                            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Você não tem quantidades suficientes do produto " + o.productCode + " em estoque");
                        }
                        else // caso tenha
                        {
                            // automaticamente decrementa o estoque em X unidades desejadas
                            productExists.productInStock = productExists.productInStock - o.productQty;

                        }

                        // ajusta o OrderID para evitar redundância no input JSON
                        o.orderID = order.orderID;

                        // busca o valor do produto na tabela de produtos
                        double prodPrice = entities.Products.Where(pro => pro.productCode == o.productCode).Select(pro => pro.productPrice).FirstOrDefault();

                        // atualiza o valor do produto
                        o.productPrice = prodPrice;

                        // atualiza o valor total do produto após as quantidades e estoque serem validados
                        o.productFinalPrice = o.productPrice * o.productQty;
                    }

                    // caso as validações passem, insere o pedido com os elementos
                    entities.Orders.Add(order);

                    // salva as altereações na entidade
                    entities.SaveChanges();

                    // devolve mensagem de sucesso e o pedido inserido na tela
                    return Request.CreateResponse(HttpStatusCode.Created, order);

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("POST")] // rotina de cadastro de um ou mais pedidos
        [Route("CadastrarPedidosMulti")]
        public HttpResponseMessage CadastrarPedidosMulti([FromBody] List<Order> ords)
        {
            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // itera cada item do input JSON
                    foreach (Order or in ords)
                    {
                        // verifica se o orderID já existe
                        if (entities.Orders.Where(o => o.orderID == or.orderID).Select(o => o).FirstOrDefault() != null)
                        {
                            // retorna código de erro caso tente cadastrar pedido já existente
                            return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Já existe um pedido com esse ID " + or.orderID + " no sistema");
                        }

                        // verifica se há itens atrelados ao pedido
                        if (or.OrdersItems.Count() == 0)
                        {
                            // retorna código de erro caso tente cadastrar pedido sem nenhum item atrelado
                            return Request.CreateErrorResponse(HttpStatusCode.Forbidden, "Não é possível adicionar um pedido sem itens.");
                        }

                        // itera cada item do input JSON
                        foreach (OrdersItem o in or.OrdersItems)
                        {
                            // confere se os items do pedido existem na tabela de produtos
                            var productExists = entities.Products.Where(x => x.productCode == o.productCode).Select(x => x).FirstOrDefault();

                            // valida se o produto existe
                            if (productExists == null)
                            {
                                // retorna mensagem de erro
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Pedido " + or.orderID + " não inserido pois o item " + o.productCode + " não existe.");
                            }

                            // valida se o produto tem estoque
                            if (productExists.productInStock == 0 || o.productQty > productExists.productInStock)
                            {
                                // retorna mensagem de erro
                                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Você não tem quantidades suficientes do produto " + o.productCode + " do pedido " + or.orderID + " em estoque");
                            }
                            else // caso haja estoque
                            {
                                // automaticamente decrementa o estoque em X unidades desejadas
                                productExists.productInStock = productExists.productInStock - o.productQty;

                            }

                            // ajusta o OrderID para evitar redundância no input JSON
                            o.orderID = or.orderID;

                            // busca o valor do produto na tabela de produtos
                            double prodPrice = entities.Products.Where(pro => pro.productCode == o.productCode).Select(pro => pro.productPrice).FirstOrDefault();

                            // atualiza o valor do produto
                            o.productPrice = prodPrice;

                            // atualiza o valor total do produto após as quantidades e estoque serem validados
                            o.productFinalPrice = o.productPrice * o.productQty;
                        }

                        // caso as validações passem, insere o pedido com os elementos
                        entities.Orders.Add(or);

                    }

                    // salva as altereações na entidade
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.Created, ords);

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("DELETE")] // rotina de exclusão de um pedido específico
        [Route("RemoverPedido/{id}")]
        public HttpResponseMessage RemoverPedido(int id)
        {
            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // busca o pedido no banco de dados
                    var ord = entities.Orders.Include("OrdersItems").Where(e => e.orderID == id).FirstOrDefault();

                    // verifica se o pedido existe no banco de dados
                    if (ord != null)
                    {
                        //verifica se há itens atrelados ao pedido
                        if (ord.OrdersItems.Count() > 0)
                        {

                            List<OrdersItem> ordItms = ord.OrdersItems.ToList();

                            // percorre os itens do pedido
                            foreach (OrdersItem oi in ordItms)
                            {
                                // removendo cada um da tabela de itens
                                entities.OrdersItems.Remove(oi);
                            }

                        }

                        // continua deletando o pedido, agora sem nenhuma relação com itens em outra tabela
                        entities.Orders.Remove(ord);

                        // save changes to the database
                        entities.SaveChanges();

                        // retorna mensagem de sucesso
                        return Request.CreateResponse(HttpStatusCode.OK, "Pedido ID " + id + " removido com sucesso.");
                    }
                    else // caso não exista
                    {
                        // retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Pedido " + id + " não encontrado.");
                    }

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }


        [AcceptVerbs("PUT")] // rotina de alteração de um único pedido
        [Route("AtualizarPedidos/{id}")]
        public HttpResponseMessage AtualizarPedido(int id, [FromBody]Order order)
        {

            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // recupera o pedido do banco de dados
                    var ord = entities.Orders.Include("OrdersItems").Where(e => e.orderID == id).FirstOrDefault();

                    // confere se o mesmo é nulo
                    if (ord == null)
                    {
                        // retorna mensagem de erro
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "O pedido " + id.ToString() + " não existe.");

                    }
                    else // caso não seja nulo
                    {
                        // atualiza o item no banco de dados, evitando nulos
                        // e mantendo a informação atual caso o campo não tenha sido informado no input JSON
                        ord.orderDate = (order.orderDate == Convert.ToDateTime("1/1/0001")) ? ord.orderDate : order.orderDate;
                        ord.orderCustID = (order.orderCustID == 0) ? ord.orderCustID : order.orderCustID;
                        ord.orderStatus = order.orderStatus ?? ord.orderStatus;
                        ord.orderShipPrice = (order.orderShipPrice == 0) ? ord.orderShipPrice : order.orderShipPrice;

                        // confere se existe algum item dentro do pedido
                        if (order.OrdersItems.Count() > 0)
                        {

                            // para cada item da coleção
                            foreach (OrdersItem oi in order.OrdersItems)
                            {
                                // tenta recuperar o item do banco
                                var ordItm = entities.OrdersItems.Where(o => (o.orderID == id) && (o.productCode == oi.productCode)).FirstOrDefault();

                                //verifica se o item desejado existe na tabela
                                if (ordItm == null)
                                {
                                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "O produto " + oi.productCode + " não está relacionado ao pedido " + id + ".");
                                }

                                // antes de atualizar, confere se existe produto em estoque
                                int qtyDiff = oi.productQty - ordItm.productQty;

                                // retorna a quantidade do produto em estoque
                                var qtyStock = entities.Products.Where(p => p.productCode == oi.productCode).Select(p => p).FirstOrDefault();

                                // se a diferença for maior que zero, ou seja, estão incluindo mais produtos
                                if (qtyDiff > 0)
                                {

                                    // confere se essa quantidade extra existe em estoque
                                    if (qtyStock.productInStock < qtyDiff)
                                    {
                                        // retorna mensagem de erro
                                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Não existem quantidades suficientes do produto " + oi.productCode + " em estoque.");
                                    }
                                    else // caso exista estoque, atualiza
                                    {
                                        // decrementa o estoque
                                        qtyStock.productInStock = qtyStock.productInStock - ordItm.productQty;
                                    }
                                }
                                else // caso estejam retirando produtos
                                {
                                    // aumenta o estoque
                                    qtyStock.productInStock = qtyStock.productInStock - qtyDiff;
                                }

                                // busca o valor do produto na tabela de produtos
                                double prodPrice = entities.Products.Where(pro => pro.productCode == oi.productCode).Select(pro => pro.productPrice).FirstOrDefault();

                                // altera as informações
                                ordItm.productQty = oi.productQty;
                                ordItm.productPrice = (oi.productPrice == 0) ? prodPrice : oi.productPrice;
                                ordItm.productFinalPrice = ordItm.productQty * ordItm.productPrice;

                            }
                        }

                        // salva as alterações
                        entities.SaveChanges();

                        // retorna mensagem de sucesso
                        return Request.CreateResponse(HttpStatusCode.OK, ord);
                    }

                }

            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [AcceptVerbs("PUT")] // rotina de alteração de dois ou mais pedidos
        [Route("AtualizarPedidosMulti")]
        public HttpResponseMessage AtualizarPedidosMulti([FromBody] List<Order> order)
        {
            try // protege a execução do bloco
            {
                // usa a entidade de conexao com banco de dados
                using (SeuManuelDBEntities entities = new SeuManuelDBEntities())
                {
                    // desabilita o lazy loading
                    entities.Configuration.LazyLoadingEnabled = false;
                    entities.Configuration.ProxyCreationEnabled = false;

                    // para cada pedido no input JSON
                    foreach (Order o in order)
                    {
                        // recupera o pedido (com seus itens) do banco de dados
                        var ord = entities.Orders.Include("OrdersItems").Where(e => e.orderID == o.orderID).FirstOrDefault();

                        // valida se o pedido é nulo
                        if (ord == null)
                        {
                            // retorna erro de não encontrado
                            return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Pedido " + o.orderID.ToString() + " não encontrado.");

                        }
                        else // caso não
                        {
                            // atualiza o pedido no banco de dados
                            ord.orderDate = (o.orderDate == Convert.ToDateTime("1/1/0001")) ? ord.orderDate : o.orderDate;
                            ord.orderCustID = (o.orderCustID == 0) ? ord.orderCustID : o.orderCustID;
                            ord.orderStatus = o.orderStatus ?? ord.orderStatus;
                            ord.orderShipPrice = (o.orderShipPrice == 0) ? ord.orderShipPrice : o.orderShipPrice;

                            // confere se existem items dentro do pedido
                            if (o.OrdersItems.Count() > 0)
                            {

                                // para cada item da coleção
                                foreach (OrdersItem oi in o.OrdersItems)
                                {
                                    // recupera o item
                                    var ordItm = entities.OrdersItems.Where(ot => (ot.orderID == o.orderID) && (ot.productCode == oi.productCode)).FirstOrDefault();

                                    //verifica se o produto desejado existe na tabela
                                    if (ordItm == null)
                                    {
                                        return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "O produto " + oi.productCode + " não está relacionado ao pedido " + o.orderID + ".");
                                    }

                                    // antes de atualizar, confere se existe produto em estoque
                                    int qtyDiff = oi.productQty - ordItm.productQty;

                                    // retorna a quantidade do produto em estoque
                                    var qtyStock = entities.Products.Where(p => p.productCode == oi.productCode).Select(p => p).FirstOrDefault();

                                    // se a diferença for maior que zero, ou seja, estão incluindo mais produtos
                                    if (qtyDiff > 0)
                                    {

                                        // confere se essa quantidade extra existe em estoque
                                        if (qtyStock.productInStock < qtyDiff)
                                        {
                                            // retorna mensagem de erro
                                            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Não existem quantidades suficientes do produto " + oi.productCode + " em estoque.");
                                        }
                                        else // caso exista estoque
                                        {
                                            // atualiza o estoque para baixo
                                            qtyStock.productInStock = qtyStock.productInStock - ordItm.productQty;
                                        }
                                    }
                                    else // caso estejam retirando produtos
                                    {
                                        // atualiza o estoque para cima
                                        qtyStock.productInStock = qtyStock.productInStock - qtyDiff;
                                    }

                                    // busca o valor do produto na tabela de produtos
                                    double prodPrice = entities.Products.Where(pro => pro.productCode == oi.productCode).Select(pro => pro.productPrice).FirstOrDefault();

                                    // altera as informações
                                    ordItm.productQty = oi.productQty;
                                    ordItm.productPrice = (oi.productPrice == 0) ? prodPrice : oi.productPrice;
                                    ordItm.productFinalPrice = ordItm.productQty * ordItm.productPrice;

                                }
                            }

                        }

                    }

                    // salva as alterações
                    entities.SaveChanges();

                    // retorna mensagem de sucesso
                    return Request.CreateResponse(HttpStatusCode.OK, "Todos os pedidos foram atualizados com sucesso.");

                }
            }
            catch (Exception ex) // trata a exceção de execução
            {
                // retorna mensagem de erro
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }

        }

    }
}
