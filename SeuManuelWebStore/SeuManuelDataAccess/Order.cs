//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeuManuelDataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            this.OrdersItems = new HashSet<OrdersItem>();
        }
    
        public int orderID { get; set; }
        public System.DateTime orderDate { get; set; }
        public int orderCustID { get; set; }
        public string orderStatus { get; set; }
        public double orderShipPrice { get; set; }
        
        [IgnoreDataMember]
        public virtual Customer Customer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrdersItem> OrdersItems { get; set; }
    }
}
