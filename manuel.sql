CREATE DATABASE SeuManuelDB;

GO

CREATE TABLE Customers (

custID int NOT NULL IDENTITY PRIMARY KEY,
custFirstName nvarchar(100) NOT NULL,
custLastName nvarchar(100) NOT NULL,
custAddress varchar(MAX) NOT NULL,

);

GO

CREATE TABLE Products (

productCode nvarchar(15) NOT NULL PRIMARY KEY,
productName nvarchar(255) NOT NULL,
productDesc varchar(MAX) NOT NULL,
productInStock int NOT NULL,
productPrice float NOT NULL
);

GO

CREATE TABLE ProductsAttributes (
attribID int NOT NULL PRIMARY KEY,
productCode nvarchar(15) NOT NULL FOREIGN KEY REFERENCES Products(productCode),
attribName nvarchar(255) NOT NULL,
attribDesc varchar(MAX) NOT NULL,
);

GO

CREATE TABLE Orders (
orderID int NOT NULL PRIMARY KEY,
orderDate date NOT NULL,
orderCustID int NOT NULL  FOREIGN KEY REFERENCES Customers(custID),
orderStatus nvarchar(255) NOT NULL,
orderShipPrice float NOT NULL,
);

GO

CREATE TABLE OrdersItems (

itemID  int NOT NULL IDENTITY PRIMARY KEY,
orderID int NOT NULL FOREIGN KEY REFERENCES Orders(orderID),
productCode nvarchar(15) NOT NULL FOREIGN KEY REFERENCES Products(productCode),
productQty int NOT NULL,
productPrice float NOT NULL,
productFinalPrice float NOT NULL

);

GO